package com.example.meetupdextra.instrumentedTest.formulario;

import com.example.meetupdextra.FormularioActivity;
import com.example.meetupdextra.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.Until;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class FormularioInstrumentedTest {

    @Rule
    public ActivityTestRule<FormularioActivity> activityRule =
            new ActivityTestRule<>(FormularioActivity.class);

    @Test
    public void fomulario_somaZero_isntrumentedTest(){ robotTestSoma("0", "0","0"); }

    @Test
    public void formulario_somaInteiros_instrymentedTest(){ robotTestSoma("10","200","210"); }

    @Test
    public void fomulario_somaNegativos_isntrumentedTest(){
        robotTestSoma("-1", "-1","-2");
    }

    private void robotTestSoma(String num1, String num2, String resultado ){
        UiDevice mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.wait(Until.hasObject(By.text("Valor A")), 5000);

        preencheCampo(R.id.editTextA, num1);
        preencheCampo(R.id.editTextB, num2);
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withId(R.id.textView3)).check(matches(withText(resultado)));
    }

    private void preencheCampo(int id, String text){
        onView(withId(id)).perform(typeText(text));
    }
}

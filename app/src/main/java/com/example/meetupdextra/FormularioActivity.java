package com.example.meetupdextra;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

import org.w3c.dom.Text;

public class FormularioActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mNum1, mNum2;
    Button mBtnCalcula;
    TextView mResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_formulario);

        //Recuperando id`s
        mNum1 = findViewById(R.id.editTextA);
        mNum2 = findViewById(R.id.editTextB);
        mResultado = findViewById(R.id.textView3);

//        mBtnCalcula.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String n1 = mNum1.getText().toString();
//                String n2 = mNum2.getText().toString();
//
//                if (!n1.isEmpty() && !n2.isEmpty()){
//                    int num1 = Integer.parseInt(mNum1.getText().toString());
//                    int num2 = Integer.parseInt(mNum2.getText().toString());
//                    int resultado = num1 + num2;
//                    mResultado.setText(""+resultado);
//                } else {
//                    Toast.makeText(getApplicationContext(), "Preencha os campos",Toast.LENGTH_LONG).show();
//                }
//
//            }
//        });
    }

    @Override
    public void onClick(View v) {

        String sN1 = mNum1.getText().toString();
        String sN2 = mNum2.getText().toString();
        int resultado;
        int n1;
        int n2;


        switch (v.getId()){
            case R.id.btnCalcular:
                if (temValor(sN1, sN2)) {
                    n1 = tratarStringParaInt(sN1);
                    n2 = tratarStringParaInt(sN2);
                    resultado = n1 + n2;
                    mResultado.setText(String.valueOf(resultado));
                }
                break;
        }
    }

    private int tratarStringParaInt(String texto) {
        return Integer.parseInt(texto);

    }

    public boolean temValor(String n1, String n2){
        if (n1.isEmpty() || n2.isEmpty()){
            Toast.makeText(getApplicationContext(), "Preencha os campos",Toast.LENGTH_LONG).show();
            return  false;
        } else {
            return true;
        }

    }
}
